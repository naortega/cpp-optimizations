# C++ Optimization Video Resources

This repository contains the resources I use for my C++ Optimization series on
my Odysee channel ([link](https://odysee.com/@nortega:7/C%2B%2B-Optimizations-Series:b?r=Cgf2A9Q55WHkdCBZhVdYdHg6bGCkmai8)).

## Building

This project is composed of various small (useless) programs. The purpose is
primarily to analyze the code and resulting binary. You will require the GNU GCC
C++ compiler (`g++`) as well as the `objdump` program. After this, you can
compile all the projects by using GNU Make and simply running `make` from the
root directory of the project.

## Copyright and License

As educational resources, it is all licensed under the [Unlicense](/LICENSE).
You may freely use all content in this repository, but with no warranty nor
guarantee. I am not responsible for your usage of these resources.
