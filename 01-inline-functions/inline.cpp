#include <cstdlib>

inline int __attribute__((always_inline)) sum(int a, int b)
{
	return a + b;
}

int main()
{
	int aux = sum(7,9);
	return EXIT_SUCCESS;
}
