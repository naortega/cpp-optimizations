CPP=g++

OBJS := 00-const-vars/const.o 00-const-vars/no-const.o 01-inline-functions/inline.o 01-inline-functions/no-inline.o 02-pass-by-ref/ref.o 02-pass-by-ref/no-ref.o

all: $(OBJS)

%.o: %.cpp
	$(CPP) $< -o $@ -c

.PHONY: clean all

clean:
	$(RM) $(OBJS)
