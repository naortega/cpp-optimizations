#include <cstdlib>

struct A
{
	char str[16];
};

int len(struct A a)
{
	int count;
	for(count = 0; a.str[count] not_eq '\0'; ++count);
	return count;
}

int main()
{
	struct A a = { .str = "Hello, World!" };
	int res = len(a);
	return EXIT_SUCCESS;
}
